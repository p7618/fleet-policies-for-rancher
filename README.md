# tefmcm - Policies for Rancher's Fleet

This repo has git "repos" Fleet examples ready to use with Rancher.

Each directory is a git repo you can add into Fleet.

- `deny-latest-label` (repo name: `denylatestlabel`): Denies the deployment of a container image with the tag latest
- `privileged-containers` (repo name: `noprivilegedcontainers`): Denies the execution of privileged containers
- `privileged-escalation` (repo name: `noprivilegedescalation`): Denies privileged escalation containers
